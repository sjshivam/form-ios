//
//  FormModel.swift
//  Forms
//
//  Created by Shivam Jaiswal on 22/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import Foundation

// TODO: implement via Codable

class FormModel: FormModelDataSource {
    let title: String
    let formMeta: [FormMeta]
    
    required init(data: [String : Any]) {
        self.title = data["title"] as? String ?? ""
        let rawForms = data["formMetaData"] as? [[String : Any]] ?? []
        let formMeta = rawForms.map { FormMeta(data: $0) }
        self.formMeta = formMeta
    }
    
    static func dummy() -> FormModel
    {
        let path = Bundle.main.path(forResource: "form", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [String : Any]
        return FormModel(data: jsonResult)
    }
}

class FormMeta: FormModelDataSource {
    let sectionHeader: String
    let forms: [Form]
    required init(data: [String : Any]) {
        self.sectionHeader = data["sectionHeader"] as? String ?? ""
        let rawForms = data["forms"] as? [[String : Any]] ?? []
        let forms = rawForms.map { Form(data: $0) }
        self.forms = forms
    }
}

class Form: FormModelDataSource, TextfieldSupportable {
    
    let editable: Bool
    let fieldType: FormCellType
    let maxLength: UInt
    let minLength: UInt
    let placeholder: String
    let postKey: String
    let errorMessage: String
    let validationType: FormValidationType
    let required: Bool
    let regx: String
    var value: String?
    var isValid = true
    
    required init(data: [String : Any]) {
        self.editable = data["editable"] as? Bool ?? true
        self.maxLength = data["maxLength"] as? UInt ?? UInt.max
        self.minLength = data["minLength"] as? UInt ?? UInt.min
        self.placeholder = data["placeholder"] as? String ?? ""
        self.value = data["value"] as? String
        self.postKey = data["postKey"] as? String ?? ""
        self.errorMessage = data["errorMessage"] as? String ?? ""
        self.required = data["required"] as? Bool ?? true
        self.regx = data["regx"] as? String ?? ""
        
        let type = data["fieldType"] as? String ?? ""
        self.fieldType = FormCellType(rawValue: type) ?? .none
        
        let validationType = data["valueType"] as? String ?? ""
        self.validationType = FormValidationType(rawValue: validationType) ?? .none
    }
}

class SubmitModel: FormModelDataSource {
    let title: String
    required init(data: [String : Any]) {
        self.title = data["title"] as? String ?? ""
    }
}

extension FormModel{
    func JSON() -> [String : Any] {
        var JSON = [String : Any]()
        for metaItem in formMeta {
            for item in metaItem.forms {
                JSON[item.postKey] = item.value
            }
        }
        return JSON
    }
    
    func vaidate() -> Bool {
        var isAllValid = true
        for metaItem in formMeta {
            for item in metaItem.forms where item.required {
                let predicate = NSPredicate(format:"SELF MATCHES %@", item.regx)
                let isValid = predicate.evaluate(with: item.value)
                item.isValid = isValid
                if !isValid{
                    isAllValid = false
                }
            }
        }
        return isAllValid
    }
}
