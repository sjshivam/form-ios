//
//  FormProtocols.swift
//  Forms
//
//  Created by Shivam Jaiswal on 22/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import Foundation

protocol FormModelDataSource {
    init(data: [String : Any])
}

protocol TextfieldSupportable {
    var editable: Bool {get}
    var fieldType: FormCellType {get}
    var maxLength: UInt {get}
    var minLength: UInt {get}
    var placeholder: String {get}
    var value: String? {get set}
    var postKey: String {get}
    var errorMessage: String {get}
    var validationType: FormValidationType {get}
    var required: Bool {get}
    var regx: String {get}
    var isValid: Bool {get set}
}
