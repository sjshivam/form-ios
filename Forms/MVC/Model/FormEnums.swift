//
//  FormEnums.swift
//  Forms
//
//  Created by Shivam Jaiswal on 22/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import Foundation

enum FormCellType: String {
    case textField
    case spinner
    case radio
    case checkBox
    case textView
    case none
}

enum FormValidationType: String {
    case text, email, number, alphanumeric, none
}
