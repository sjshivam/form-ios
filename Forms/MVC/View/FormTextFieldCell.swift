//
//  FormTextFieldCell.swift
//  Forms
//
//  Created by Shivam Jaiswal on 21/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import UIKit

protocol FormTextFieldCellDelegate: AnyObject {
    func textFieldCell(_ cell:FormTextFieldCell, didEndEditing textField: FormTextField)
    func textFieldCellDidTapDownButton(_ cell:FormTextFieldCell)
    func textFieldCellDidTapPreviousButton(_ cell:FormTextFieldCell)
    func textFieldCellDidTapNextButton(_ cell:FormTextFieldCell)
}

class FormTextFieldCell: FormTableViewCell
{
    private static let textFieldHeight: CGFloat = 44
    private static let errorPadding: CGFloat = 2
    private static let errorLableFont = UIFont.light(size: 10)
    
    private lazy var errorLabel: UILabel = {
        let lbl = UILabel.init(frame: .zero)
        lbl.textColor = UIColor.red
        lbl.numberOfLines = 0
        lbl.font = FormTextFieldCell.errorLableFont
        return lbl
    }()
    
    let textField = FormTextField()
    private lazy var toolBar: FormKeyboardToolBar = {
        let bar = FormKeyboardToolBar()
        bar.delegate = self
        return bar
    }()
    
    override func setup() {
        super.setup()
        
        textField.font = UIFont.regular(size: 16);
        textField.autocorrectionType = .no;
        textField.clearButtonMode = .never;
        textField.rightViewMode = .never;
        textField.delegate = self;
        textField.inputAccessoryView = toolBar;

        contentView.addSubview(textField)
        contentView.addSubview(errorLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let rect = contentView.bounds;
        var x,y: CGFloat
        var tSize: CGSize
        let awailableWidth = rect.size.width - 2 * sidePadding
        
        // textField
        tSize = CGSize.init(width: awailableWidth , height: FormTextFieldCell.textFieldHeight)
        x = sidePadding
        y = 0;
        textField.frame = CGRect(x, y, tSize)
        
        // error Label
        tSize = sizeFor(errorLabel.text, font: errorLabel.font, width: awailableWidth)
        x = sidePadding
        y = textField.frame.maxY + FormTextFieldCell.errorPadding
        errorLabel.frame = CGRect(x, y, tSize)
    }
    
    // MARK: Public Setter
    
    weak var delegate: FormTextFieldCellDelegate?
    
    var form: TextfieldSupportable?{
        didSet{
            guard let form = self.form else { return }
            setupTextFieldStyle(validation: form.validationType)
            textField.placeholder = form.placeholder
            textField.text = form.value
            errorLabel.text = form.isValid ? nil : form.errorMessage
            setNeedsLayout()
        }
    }
    
    // MARK: Utils
    func setupTextFieldStyle(validation: FormValidationType){
        switch validation {
        case .text,.alphanumeric:
            textField.keyboardType = .default;
            textField.autocapitalizationType = .words;
        case .email:
            textField.keyboardType = .emailAddress;
            textField.autocapitalizationType = .none
        case .number:
            textField.keyboardType = .phonePad;
            textField.autocapitalizationType = .none
        default:
            break;
        }
    }
    
    static func height(for width: CGFloat, form: TextfieldSupportable) -> CGFloat {
        let height = defaultHeight()
        if form.isValid {
            return height
        }else{
            let awailableWidth = width - 2 * sidePadding
            let errorHeight = sizeFor(form.errorMessage, font: errorLableFont, width: awailableWidth).height
            return height + errorHeight + 2 * errorPadding
        }
    }
}

extension FormTextFieldCell: FormKeyboardToolBarDelegate
{
    func toolbar(_ bar: FormKeyboardToolBar, didTapDownButton: UIBarButtonItem) {
        delegate?.textFieldCellDidTapDownButton(self)
    }
    
    func toolbar(_ bar: FormKeyboardToolBar, didTapPreviousButton: UIBarButtonItem) {
        delegate?.textFieldCellDidTapPreviousButton(self)
    }
    
    func toolbar(_ bar: FormKeyboardToolBar, didTapNextButton: UIBarButtonItem) {
        delegate?.textFieldCellDidTapNextButton(self)
    }
}

extension FormTextFieldCell: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textField = textField as? FormTextField else { return }
        delegate?.textFieldCell(self, didEndEditing: textField)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        errorLabel.text = nil
        form?.isValid = true
        updateHeight()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let tfText = textField.text as NSString?, let form = form else { return false }
        let text = tfText.replacingCharacters(in: range, with: string)
        
        if form.validationType == .none {
            return true
        }
        
        if text.count > form.maxLength {
            return false
        }
        
        switch form.validationType {
        case .alphanumeric:
            return text.isAlphanumeric(withSpace: true)
        case .number:
            return text.isDigitOnly()
        case .text:
            return text.isTextOnly(withSpace: true)
        default:
            return true
        }
    }
}
