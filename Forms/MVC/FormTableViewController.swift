//
//  FormTableViewController.swift
//  Forms
//
//  Created by Shivam Jaiswal on 21/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import UIKit

class FormTableViewController: UITableViewController {

    let model = FormModel.dummy()
    var invalidIndexPath = [IndexPath]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorStyle = .none
        self.title = model.title
        registerCells()
        
        let save = UIBarButtonItem.init(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))
        navigationItem.rightBarButtonItem = save
    }

    @objc func saveTapped() {
        self.view.endEditing(true)
        guard model.vaidate() else {
            tableView.reloadData()
            return
        }
        print(model.JSON())
    }
    
    func registerCells(){
        tableView.register(FormTextFieldCell.self, forCellReuseIdentifier: FormCellType.textField.rawValue)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return model.formMeta.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.formMeta[section].forms.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let form = model.formMeta[indexPath.section].forms[indexPath.row]
        return FormTextFieldCell.height(for: tableView.frame.width, form: form)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return model.formMeta[section].sectionHeader
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let form = model.formMeta[indexPath.section].forms[indexPath.row]
        switch form.fieldType {
        case .textField:
            let cell = textFieldCell(tableView, cellForRowAt: indexPath, form: form)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func textFieldCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, form: Form) -> FormTextFieldCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: form.fieldType.rawValue, for: indexPath) as! FormTextFieldCell
        cell.form = form
        cell.indexpath = indexPath
        cell.previouseIndexPath = indexPath.previous
        cell.nextIndexPath = indexPath.next
        cell.delegate = self
        
        if tableView.isFirstRow(for: indexPath)
        {
            if tableView.isFirstSecton(for: indexPath)
            {
                cell.previouseIndexPath = nil
            }
            else
            {
                let row = tableView.numberOfRows(inSection: indexPath.section - 1)
                cell.previouseIndexPath = IndexPath(row: row - 1, section: indexPath.section - 1)
            }
        }
        
        if tableView.isLastRow(for: indexPath)
        {
            if tableView.isLastSection(for: indexPath)
            {
                cell.nextIndexPath = nil
            }
            else
            {
                cell.nextIndexPath = indexPath.nextSection
            }
        }
        
        return cell
    }
}

extension FormTableViewController: FormTextFieldCellDelegate
{
    func textFieldCell(_ cell: FormTextFieldCell, didEndEditing textField: FormTextField) {
        guard let indexPath = cell.indexpath else { return }
        let form = model.formMeta[indexPath.section].forms[indexPath.row]
        form.value = textField.text
    }
    
    func textFieldCellDidTapDownButton(_ cell: FormTextFieldCell) {
        cell.textField.endEditing(true)
    }
    
    func textFieldCellDidTapPreviousButton(_ cell: FormTextFieldCell) {
        activateTextField(at: cell.previouseIndexPath)
    }
    
    func textFieldCellDidTapNextButton(_ cell: FormTextFieldCell) {
        activateTextField(at: cell.nextIndexPath)
    }
    
    func activateTextField(at indexPath: IndexPath?)
    {
        guard let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? FormTextFieldCell  else{
            self.view.endEditing(true)
            return
        }
        cell.textField.becomeFirstResponder()
    }
}
