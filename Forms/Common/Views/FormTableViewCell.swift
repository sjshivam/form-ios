//
//  FormTableViewCell.swift
//  Forms
//
//  Created by Shivam Jaiswal on 21/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import UIKit

class FormTableViewCell: UITableViewCell {

    var indexpath: IndexPath?
    var nextIndexPath: IndexPath?
    var previouseIndexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        selectionStyle = .none
    }
    
    static func defaultHeight() -> CGFloat{
        return 64
    }
}
