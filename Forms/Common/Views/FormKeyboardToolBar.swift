//
//  FormKeyboardToolBar.swift
//  Forms
//
//  Created by Shivam Jaiswal on 22/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import UIKit

protocol FormKeyboardToolBarDelegate: UIToolbarDelegate {
    func toolbar(_ bar: FormKeyboardToolBar, didTapDownButton: UIBarButtonItem)
    func toolbar(_ bar: FormKeyboardToolBar, didTapPreviousButton: UIBarButtonItem)
    func toolbar(_ bar: FormKeyboardToolBar, didTapNextButton: UIBarButtonItem)
}

class FormKeyboardToolBar: UIToolbar {
    
    override var delegate: UIToolbarDelegate?{
        didSet{
            barDelegate = delegate as? FormKeyboardToolBarDelegate
        }
    }
    
    private weak var barDelegate: FormKeyboardToolBarDelegate?
    
    override init(frame: CGRect) {
        let rect = CGRect.init(x: 0, y: 0, width: 0, height: 44)
        super.init(frame: rect)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup()  {
        tintColor = UIColor.lightGray
        barStyle = .default

        let flexSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let fixed = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixed.width = 32.0
        
        let left = UIBarButtonItem.init(image: UIImage.init(named: "arrow.left"), style: .plain, target: self, action: #selector(toolBarPrevButtonPressed(_:)))
        let right = UIBarButtonItem.init(image: UIImage.init(named: "arrow.right"), style: .plain, target: self, action: #selector(toolBarNextButtonPressed(_:)))
        let down = UIBarButtonItem.init(image: UIImage.init(named: "keyboard_down"), style: .plain, target: self, action: #selector(toolBarDoneButtonPressed(_:)))

        let barItems = [down, flexSpace, left, fixed, right];
        
        setItems(barItems, animated: true)
    }
    
    @objc private func toolBarDoneButtonPressed(_ sender: UIBarButtonItem){
        barDelegate?.toolbar(self, didTapDownButton: sender)
    }

    @objc private func toolBarPrevButtonPressed(_ sender: UIBarButtonItem){
        barDelegate?.toolbar(self, didTapPreviousButton: sender)
    }

    @objc private func toolBarNextButtonPressed(_ sender: UIBarButtonItem){
        barDelegate?.toolbar(self, didTapNextButton: sender)
    }
}
