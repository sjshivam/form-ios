//
//  Constants.swift
//  Forms
//
//  Created by Shivam Jaiswal on 21/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

// global space
let sidePadding: CGFloat = 15.0

func GRAY(white: CGFloat) -> UIColor {
    return UIColor(white: white, alpha: 1)
}

func RGB(red: Int, green: Int, blue: Int) -> UIColor {
    return UIColor(red: red, green: green, blue: blue)
}

func CenteredOrigin(_ x: CGFloat, _ y: CGFloat) -> CGFloat {
    return floor((x - y)/2.0)
}

func sizeFor(_ attributedString: NSAttributedString?, width: CGFloat) -> CGSize
{
    guard let string = attributedString else { return .zero }
    var size = string.boundingRect(with: CGSize.init(width: width, height: .greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil).size
    size.width = ceil(size.width)
    size.height = ceil(size.height)
    return size
}

func sizeFor(_ string: String?, font: UIFont?, width: CGFloat) -> CGSize
{
    guard let string = string, let font = font else { return .zero }
    var size = string.boundingRect(with: CGSize.init(width: width, height: .greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes:[NSAttributedString.Key.font : font], context: nil).size
    size.width = ceil(size.width)
    size.height = ceil(size.height)
    return size
}


