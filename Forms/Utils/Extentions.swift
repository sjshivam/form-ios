//
//  Extentions.swift
//  Forms
//
//  Created by Shivam Jaiswal on 21/01/19.
//  Copyright © 2019 AppStreet Software Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func lookForSuperviewOfType<T: UIView>(type: T.Type) -> T? {
        guard let view = self.superview else {
            return nil
        }
        return (view as? T) ?? view.lookForSuperviewOfType(type: T.self)
    }
}

extension CGRect {
    init(_ x: CGFloat, _ y: CGFloat, _ size: CGSize) {
        self.init(origin: CGPoint.init(x: x, y: y), size: size)
    }
}

extension String {
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func isAlphanumeric(withSpace: Bool = false) -> Bool {
        var set = CharacterSet.alphanumerics
        if withSpace {
            set = set.union(CharacterSet.whitespaces)
        }
        return rangeOfCharacter(from: set.inverted) == nil
    }
    
    func isDigitOnly() -> Bool {
        let set = CharacterSet.decimalDigits
        return rangeOfCharacter(from: set.inverted) == nil
    }
    
    func isTextOnly(withSpace: Bool = false) -> Bool {
        var set = CharacterSet.newlines
        if withSpace {
            set.insert(charactersIn: " ")
        }
        return rangeOfCharacter(from: set.inverted) == nil
    }
}

extension FloatingPoint where Self == Double{
    var stringValue: String {return String(self)}
}

extension Int{
    var boolValue: Bool { return self != 0 }
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last!
    }
    
    class var className: String {
        return String(describing: self).components(separatedBy: ".").last!
    }
}

extension UIImageView {
    func setTintColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UITableViewCell{
    var tableView: UITableView? {return self.lookForSuperviewOfType(type: UITableView.self)}
    
    func updateHeight(){
        tableView?.beginUpdates()
        tableView?.endUpdates()
    }
}

extension UITableView{
    
    func isFirstRow(for indexpath: IndexPath) -> Bool {
        return indexpath.row == 0
    }
    
    func isLastRow(for indexpath: IndexPath) -> Bool {
        return (self.numberOfRows(inSection: indexpath.section) - 1 ) == indexpath.row
    }
    
    func isLastSection(for indexpath: IndexPath) -> Bool {
        return ((self.numberOfSections - 1) == indexpath.section)
    }
    
    func isFirstSecton(for indexpath: IndexPath) -> Bool {
        return indexpath.section == 0
    }
    
    func updateHeight(){
        self.beginUpdates()
        self.endUpdates()
    }
}

extension UIFont {
    
    static func light(size: CGFloat) -> UIFont  {
        return UIFont(name:"Helvetica-Light", size: size)!
    }
    
    static func regular(size: CGFloat) -> UIFont  {
        return UIFont(name:"Helvetica", size: size)!
    }
}

@nonobjc extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
    static var appGreyColor: UIColor {
        return UIColor(red: 102, green: 102, blue: 102)
    }
    
    static var appLightGreyColor: UIColor {
        return UIColor(red: 172, green: 184, blue: 191)
    }
    
    static var appBlackColor: UIColor {
        return UIColor(red: 62, green: 62, blue: 62)
    }
    
    static var appBlueColor: UIColor {
        return UIColor(red: 4, green: 169, blue: 244)
    }
    
    static var appDarkBlueColor: UIColor {
        return UIColor(red: 2, green: 119, blue: 189)
    }
    
    static var appRedColor: UIColor {
        return UIColor(red: 237, green: 28, blue: 36)
    }
    
    static var appGreenColor: UIColor {
        return UIColor(red: 139, green: 195, blue: 74)
    }
    
    static var appDarkOrangeColor: UIColor {
        return UIColor(red: 251, green: 88, blue: 37)
    }
    
    static var appOffWhiteColor: UIColor {
        return GRAY(white: 0.95)
    }
    static var appMainBGColor: UIColor {
        return GRAY(white: 0.92)
    }
    static var appDisabledGrayColor: UIColor {
        return GRAY(white: 0.85)
    }
    static var appSeparatorColor: UIColor {
        return GRAY(white: 0.84)
    }
    
    static var appGreenTextColor: UIColor {
        return UIColor(red: 0, green: 154, blue: 0)
    }
    static var appDarkTextColor: UIColor {
        return GRAY(white: 0.2)
    }
    static var appDarkGrayColor: UIColor {
        return GRAY(white: 0.4)
    }
    static var appGrayTextColor: UIColor {
        return GRAY(white: 0.5)
    }
    static var appLightGrayColor: UIColor {
        return GRAY(white: 0.6)
    }
    static var appExtraLightGrayColor: UIColor {
        return GRAY(white: 0.8)
    }
    static var tableViewBackgroundColor: UIColor {
        return RGB(red: 147, green: 147, blue: 147)
    }
}

extension IndexPath
{
    var next: IndexPath {
        return IndexPath.init(row: row + 1, section: section)
    }
    
    var previous: IndexPath {
        return IndexPath.init(row: row - 1, section: section)
    }
    
    var nextSection: IndexPath {
        return IndexPath.init(row: 0, section: section + 1)
    }
    
    var previousSection: IndexPath {
        return IndexPath.init(row: 0, section: section - 1)
    }
}
